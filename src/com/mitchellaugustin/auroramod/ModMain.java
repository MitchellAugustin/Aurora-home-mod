package com.mitchellaugustin.auroramod;

import java.io.IOException;
import java.util.TimerTask;

import javax.swing.JPanel;

import com.mitchellaugustin.aurora.home.Main;
import com.mitchellaugustin.aurora.interpreter.Synonyms;
import com.mitchellaugustin.aurora.layout.CustomMasterWindow;
import com.mitchellaugustin.aurora.layout.EpochNode;
import com.mitchellaugustin.aurora.layout.WeatherNode;
import com.mitchellaugustin.aurora.microphone.Recognizer;

/**
 * Aurora for Home Automation Software Framework
 * @author Mitchell Augustin
 * � Copyright 2016 Aurora and all related source code are property of Mitchell Augustin.
 * Any code under the com.mitchellaugustin.aurora package(s) are copyrighted by Mitchell Augustin.
 * 
 * This project contains all of the source code responsible for operating the speech to 
 * text, text to speech, and Aurora Interpreter functions of the Aurora Home Unit, or AHU. 
 * This software comes with NO WARRANTY, and any reproduction or redistribution of it 
 * without the written consent of Mitchell Augustin is strictly prohibited.
 * For full license details, see http://mitchellaugustin.com/aurora-eula.html
 *
 */
public class ModMain extends Main {
	public ModMain(boolean getWeatherData, boolean getTextData, boolean useCustomWindow) {
		super(getWeatherData, getTextData, useCustomWindow);
	}
	
	@Override
	public void recognize(String file, boolean overridden) throws IOException{
		//Run the recorded file through the IBM recognizer & retrieve a transcription
		String request = Recognizer.recognizeIBM(file).toLowerCase();
		
		//Re-format the query using Aurora's synonym replacement method
		request = Synonyms.formatQuery(request);
		
		//At this point, you should use your own method to override Aurora's default interpreter.
		//In this example, I simply search the request for the word "modified". If it is in the query,
		//Aurora will say "Aurora modular injection successful." If your mod keyword(s) are not recognized,
		//I suggest that you call the default recognizer method so that Aurora at least returns a response
		//of some kind.
		if(request.contains("modified")){
			super.speak("Aurora modular injection successful.");
			
			//Do your mod's work here...
			
			//Invokes the recognize method of the superclass with the audio file and a "true" parameter.
			//The boolean value should be true if your modified interpreter returned a response.
			super.recognize(file, true);
		}
		else{
			//Invokes the recognize method of the superclass with the audio file and a "true" parameter.
			//The boolean value should be false if your mod didn't return a response & you want to pull Aurora's default response.
			super.recognize(file, false);
		}

		
	}

	public static void main(String[] args){
				//Your CustomMasterWindow object
				CustomMasterWindow win = new CustomMasterWindow();

				//Our two custom nodes should be instantiated here.
				JPanel node1 = new WeatherNode("Columbia, IL");
				JPanel node2 = new EpochNode();
				
				//Define a TimerTask for the window to run every x seconds
				class update extends TimerTask{
					@Override
					public void run(){
						win.lowPanel.removeAll();
						//Re-initialize your panels here.
						JPanel newNode1 = new EpochNode();
						JPanel newNode2 = new EpochNode();
						win.initializeCustomNodes(newNode1, newNode2, null, 600000);
					}
				};
				
				//This should be identical to the method call in your TimerTask's run() method.
				//If any of your nodes require internet access, be sure to allow enough time for it to reload in both calls.
				win.initializeCustomNodes(node1, node2, new update(), 600000);
				
				//Initialize the backbone class of Aurora home. 
				//Note: If you are using custom nodes, I suggest that you disable weather and text data retrieval.
				@SuppressWarnings("unused")
				ModMain main = new ModMain(false, false, true);
	}
}
